<html>
<head>
    <title>Book Store</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="/frontend/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
</head>
<body>
<h1 class="text-center">Welcome to Book Store</h1>
<div class="container" align="center">
    <a href="/backend/add.php"><button class="btn btn-primary">Add</button></a>
    <a href="/backend/books.php"><button class="btn btn-primary">Books</button></a>
    <a href="/backend/about.php"><button class="btn btn-primary">Contacts</button></a>
</div>
<script src="https://code.jquery.com/jquery.js"></script>
<script src="/frontend/bootstrap/js/bootstrap.min.js"></script>
<div class="container" align="center"style="padding-top: 50px">
    <h2>Классы, объекты и т.д.</h2>
<?php
class ShopProduct implements Chargeable{
    const AVAILABLE            = 0;
    public $title            = "Стандартный товар";
    public $producerMainName = "Фамилия автора";
    public $producerFirstName = "Имя автора";
    public $price            = 0;

    function __construct($title,$firstName,$mainName,$price)
    {
        $this->title            = $title;
        $this->producerMainName = $firstName;
        $this->producerFirstName= $mainName;
        $this->price            = $price;
    }
    public function getPrice()
    {
        return ($this->price);// TODO: Implement getPrice() method.
    }
    function getProducer(){
    return "{$this->producerMainName}";
}
};
$product1 = new ShopProduct("Собачье сердце", "Михаил", "Булгаков", "5,99");
Class StaticExample {
    static public $aNum = 0;
    static public function sayHello(){
        self::$aNum++;
        print "Пример работы статического элемента(" . self::$aNum . ")\n";
    }
}
StaticExample::sayHello();

class AbstractClass{
    function abstractFunction(){
        die("AbstractClass::abstractFunction() - абстрактная функция! \n" );
    }
}
interface Chargeable{
    public function getPrice();
}
print "Пример работы константы" .ShopProduct::AVAILABLE;
?>
</div></body></html>
